import { Body, Controller, Get, Post } from '@nestjs/common';
import { UserService } from '../services/users.service';
import { CreateUserDto } from '../dto/user.dto';
import { User } from '../entities/user.entity';

@Controller('users')
export class UsersController {
  constructor(private userService: UserService) {}

  @Get()
  async findAll(): Promise<User[]> {
    return this.userService.findAll();
  }
  @Post('create')
  async create(@Body() payload: CreateUserDto) {
    return await this.userService.create(payload);
  }
}
